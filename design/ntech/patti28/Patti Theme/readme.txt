Thank you for using Patti theme for your project. 

As you can see, there are 2 .zip archives in this folder. 
The main theme is in "patti.zip". This is the theme you want to install.

If you want to use a child theme too along with the main theme, firstly install "patti.zip" and then install patti-child.zip" too. For more informations about child themes, visit http://codex.wordpress.org/Child_Themes

Thank you!